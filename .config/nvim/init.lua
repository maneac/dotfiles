vim.cmd([[
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
	silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall --sync | source ~/.config/nvim/init.vim
endif
]])

local Plug = vim.fn['plug#']

vim.call('plug#begin', '~/.config/nvim/plugged')

Plug('ap/vim-buftabline')
Plug('chriskempson/base16-vim')
Plug('airblade/vim-gitgutter')
Plug('airblade/vim-rooter')

Plug('junegunn/fzf', {['do'] = '-> fzf#install()'})
Plug('junegunn/fzf.vim')

Plug('fatih/vim-go', {['do'] = ':GoUpdateBinaries'})

Plug('neovim/nvim-lspconfig')
Plug('nvim-lua/lsp_extensions.nvim')
Plug('nvim-lua/completion-nvim')
Plug('nvim-treesitter/nvim-treesitter', {['do'] = ':TSUpdate'})
Plug('nvim-treesitter/completion-treesitter')

Plug('rust-lang/rust.vim', {['for'] = 'rust'})
Plug('rhysd/vim-clang-format')

vim.call('plug#end')

vim.g.t_Co = 256
vim.opt.background = 'dark'
vim.g.base16colorspace = 256
vim.cmd('source ~/.vimrc_background')

--[[

  #########
 | Plugins |
  #########

--]]

-- Treesitter
require('nvim-treesitter.configs').setup({
	ensure_installed = {
		'bash',
		'bibtex',
		'c',
		'cmake',
		'comment',
		'css',
		'dockerfile',
		'dot',
		'go',
		'gomod',
		'html',
		'javascript',
		'json',
		'jsonc',
		'kotlin',
		'latex',
		'lua',
		'python',
		'r',
		'regex',
		'rust',
		'toml',
		'tsx',
		'typescript',
		'vim',
		'vue',
		'yaml',
	},
	highlight = {
		enable = true,
		additional_vim_regex_highlighting = false,
	},
	indent = {
		enable = true,
	},
})

vim.g.completion_chain_complete_list = {
	default = {
		default = {
	{ complete_items = {'lsp', 'snippets' } },
			},
		string = {
			{ mode = 'file' },
		},
	},
	lua = {
		{ complete_items = { 'ts' } },
	},
}

local format_on_save = {}

function _G.format_and_save(language)
	local fmt = format_on_save[language]
	if fmt then fmt() else vim.lsp.buf.formatting() end
	vim.cmd('write')
end

-- LSP
local on_attach = function(language)
	local attach_fn = function(client, bufnr)
		local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end
		local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
		buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')
		print(language)
		local opts = { noremap = true, silent = true }
		buf_set_keymap('n', 'gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
		buf_set_keymap('n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
		buf_set_keymap('n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
		buf_set_keymap('n', 'gi', '<Cmd>lua vim.lsp.buf.implementation()<CR>', opts)
		buf_set_keymap('n', '<C-k>', '<Cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
		buf_set_keymap('n', '<space>D', '<Cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
		buf_set_keymap('n', '<space>rn', '<Cmd>lua vim.lsp.buf.rename()<CR>', opts)
		buf_set_keymap('n', '<F2>', '<Cmd>lua vim.lsp.buf.rename()<CR>', opts)
		buf_set_keymap('n', '<space>a', '<Cmd>lua vim.lsp.buf.code_action()<CR>', opts)
		buf_set_keymap('n', 'gr', '<Cmd>lua vim.lsp.buf.references()<CR>', opts)
		buf_set_keymap('n', 'e', '<Cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
		buf_set_keymap('n', '[d', '<Cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
		buf_set_keymap('n', ']d', '<Cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
		buf_set_keymap('n', '<space>l', '<Cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
		buf_set_keymap('n', '<space>f', '<Cmd>lua vim.lsp.buf.formatting()<CR>', opts)
		buf_set_keymap('n', 'W', string.format('<Cmd>lua format_and_save(%s)<CR>', language), opts)

		require('completion').on_attach(client)
	end
	return attach_fn
end

local lspconfig = require('lspconfig')

-- Lua LSP

-- set the path to the sumneko installation; if you previously installed via the now deprecated :LspInstall, use
local sumneko_root_path = vim.fn.stdpath('cache')..'/nlua/sumneko_lua/lua-language-server'
local sumneko_binary = "/usr/bin/lua-language-server"

local runtime_path = vim.split(package.path, ';')
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

lspconfig.sumneko_lua.setup {
  cmd = {sumneko_binary, "-E", sumneko_root_path .. "/main.lua"};
  settings = {
    Lua = {
      runtime = {
        -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
        version = 'LuaJIT',
        -- Setup your lua path
        path = runtime_path,
      },
      diagnostics = {
        -- Get the language server to recognize the `vim` global
        globals = {'vim'},
      },
      workspace = {
        -- Make the server aware of Neovim runtime files
        library = vim.api.nvim_get_runtime_file("", true),
      },
      -- Do not send telemetry data containing a randomized but unique identifier
      telemetry = {
        enable = false,
      },
    },
  },
  on_attach = on_attach('lua'),
}

-- Rust
lspconfig.rust_analyzer.setup {
	settings = {
		["rust-analyzer"] = {
			assist = {
				importGranularity = "module",
				importPrefix = "by_self",
				importGroup = true,
				allowMergingIntoGlobImports = true,
			},
			callInfo = {
				full = true,
			},
			cargo = {
				autoreload = true,
				allFeatures = true,
				loadOutDirsFromCheck = true,
				runBuildScripts = true,
			},
			checkOnSave = {
				command = "clippy",
				enable = true,
			},
			completion = {
				addCallArgumentSnippets = true,
				addCallParenthesis = true,
				postfix = { enable = true },
				autoimport = { enable = true },
				autoself = { enable = true },
			},
			diagnostics = {
				enable = true,
			},
			files = {
				watcher = "client",
			},
			hightlightRelated = {
				references = true,
				exitPoints = true,
				breakPoints = true,
				yieldPoints = true,
			},
			highlighting = {
				strings = true,
			},
			hover = {
				documentation = true,
				linksInHover = true,
			},
			hoverActions = {
				debug = true,
				enable = true,
				gotoTypeDef = true,
				implementations = true,
				run = true,
			},
			inlayHints = {
				chainingHints = true,
				maxLength = 25,
				parameterHints = true,
				typeHints = true,
			},
			joinLines = {
				joinElseIf = true,
				removeTrailingComma = true,
				unwrapTrivialBlock = true,
				joinAssignments = true,
			},
			lens = {
				debug = true,
				enable = true,
				run = true,
				forceCustomCommands = true,
			},
			notifications = {
				cargoTomlNotFound = true,
			},
			procMacro = {
				enable = true,
			},
			workspace = {
				symbol = {
					search = {
						scope = "workspace",
						kind = "only_types",
					},
				},
			},
		},
	},
	on_attach = on_attach('rust'),
	flags = {
		debounce_text_changes = 150,
	},
}

vim.g.rustfmt_autosave = 1

vim.cmd([[
autocmd CursorHold,CursorHoldI *.rs :lua require'lsp_extensions'.inlay_hints{ }
]])

-- TOML LSP
lspconfig.taplo.setup {
	config = {
		formatter = {
			trailingNewline = true,
			reorderKeys = true,
		},
	},
	on_attach = on_attach('yaml'),
}

-- Go LSP
lspconfig.gopls.setup {
	settings = {
		gopls = {
			formatting = {
			},
		},
	},
	on_attach = on_attach('go'),
}


-- C / C++ LSP
lspconfig.ccls.setup {
	init_options = {
		cache = {
			directory = vim.fn.expand('$HOME') .. '.cache/.ccls-cache',
		},
	},
	on_attach = on_attach('c'),
}

vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
	vim.lsp.diagnostic.on_publish_diagnostics, {
		virtual_text = true,
		signs = true,
		update_in_insert = true,
	}
)

--[[

  ########
 | Config |
  ########

--]]

vim.g.mapleader = ' '

vim.opt.autowrite = true
vim.opt.number = true

vim.opt.hidden = true
vim.opt.confirm = true

vim.opt.sessionoptions:remove({'options'})
vim.cmd('syntax enable')
vim.cmd('filetype plugin indent on')
vim.opt.omnifunc = "syntaxcomplete#Complete"

vim.opt.autoindent = true
vim.opt.timeoutlen = 100
vim.opt.encoding = 'utf-8'
vim.opt.showmode = false
vim.opt.wrap = false
vim.opt.joinspaces = false
vim.opt.signcolumn = 'yes'

vim.opt.splitright = true
vim.opt.splitbelow = true

vim.opt.undodir = vim.fn.expand('$HOME') .. '/.vimdid'
vim.opt.undofile = true

vim.opt.wildmenu = true
vim.opt.wildmode = 'list:longest'
vim.opt.wildignore = '.hg,.svn,*~,*.png,*.jpg,*.gif,*.settings,Thumbs.db,*.min.js,*.swp,publish/*,intermediate/*,*.o,*.hi,Zend,vendor'

vim.opt.shiftwidth = 8
vim.opt.softtabstop = 8
vim.opt.tabstop = 8
vim.opt.expandtab = false

vim.opt.formatoptions = 'tcrqnb'

vim.opt.incsearch = true
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.gdefault = true

vim.opt.foldenable = false
vim.opt.lazyredraw = true
vim.opt.diffopt:append({
	iwhite = true,
	['algorith:patience'] = true,
	['indent-heuristic'] = true,
})

vim.opt.listchars = 'nbsp:¬,extends:»,precedes:«,trail:•'
vim.opt.showcmd = true

vim.opt.completeopt = {'menuone', 'noinsert', 'noselect'}
vim.opt.cmdheight = 2
vim.opt.updatetime = 100
vim.opt.shortmess:append({
	c = true,
})

vim.opt.mouse = 'a'

-- vim.cmd('autocmd BufWritePost,FileWritePost ~/.config/nvim/init.lua source ~/.config/nvim/init.lua')

--[[

  ##########
 | Keyboard |
  ##########

--]]
vim.api.nvim_set_keymap('n', 'n', 'nzz', { silent = true, noremap = true })
vim.api.nvim_set_keymap('n', 'N', 'Nzz', { silent = true, noremap = true })
vim.api.nvim_set_keymap('n', '*', '*zz', { silent = true, noremap = true })
vim.api.nvim_set_keymap('n', '#', '#zz', { silent = true, noremap = true })
vim.api.nvim_set_keymap('n', 'g*', 'g*zz', { silent = true, noremap = true })

vim.api.nvim_set_keymap('n', '?', '?\\v', { noremap = true })
vim.api.nvim_set_keymap('n', '/', '/\\v', { noremap = true })
vim.api.nvim_set_keymap('c', '%s/', '%sm/', { noremap = true })

vim.api.nvim_set_keymap('n', 'j', 'gjzz', { noremap = true })
vim.api.nvim_set_keymap('n', 'k', 'gkzz', { noremap = true })
vim.api.nvim_set_keymap('n', '<C-j>', 'gj', { noremap = true })
vim.api.nvim_set_keymap('n', '<C-k>', 'gk', { noremap = true })
vim.api.nvim_set_keymap('n', '<S-g>', 'Gzz', { noremap = true })

vim.api.nvim_set_keymap('n', '<Leader>w', ':w<CR>', { noremap = true })
vim.api.nvim_set_keymap('n', '<Leader>q', ':bp|bd #<CR>', { noremap = true })
vim.api.nvim_set_keymap('n', '<Left>', ':bp<CR>', { noremap = true })
vim.api.nvim_set_keymap('n', '<Right>', ':bn<CR>', { noremap = true })

vim.api.nvim_set_keymap('n', '<S-Left>', ':cp<CR>', { noremap = true })
vim.api.nvim_set_keymap('n', '<S-Right>', ':cn<CR>', { noremap = true })

vim.api.nvim_set_keymap('t', '<Esc>', '<C-\\><C-n>', { noremap = true })

vim.api.nvim_set_keymap('n', '<C-h>', ':nohlsearch<CR>', { noremap = true })
vim.api.nvim_set_keymap('v', '<C-h>', ':nohlsearch<CR>', { noremap = true })

vim.api.nvim_set_keymap('', 'H', '^', {})
vim.api.nvim_set_keymap('', 'L', '$', {})

vim.api.nvim_set_keymap('', '<Leader>s', ':Rg<CR>', { noremap = true })
vim.g.fzf_layout = { down = '~20%' }

-- vim.api.nvim_set_keymap('n', '<Up>','<NOP>', { noremap = true })
-- vim.api.nvim_set_keymap('i', '<Up>','<NOP>', { noremap = true })
-- vim.api.nvim_set_keymap('n', '<Down>','<NOP>', { noremap = true })
-- vim.api.nvim_set_keymap('i', '<Down>','<NOP>', { noremap = true })
-- vim.api.nvim_set_keymap('i', '<Left>','<NOP>', { noremap = true })
-- vim.api.nvim_set_keymap('i', '<Right>','<NOP>', { noremap = true })

vim.api.nvim_set_keymap('n', 'j','gj', { noremap = true })
vim.api.nvim_set_keymap('n', 'k','gk', { noremap = true })

local function t(str)
	return vim.api.nvim_replace_termcodes(str, true, true, true)
end

function _G.smart_tab()
	return vim.fn.pumvisible() == 1 and t('<C-n>') or t('<Tab>')
end

function _G.smart_untab()
	return vim.fn.pumvisible() == 1 and t('<C-p>') or t('<S-Tab>')
end

vim.api.nvim_set_keymap('i', '<Tab>', 'v:lua.smart_tab()', { noremap = true, expr = true })
vim.api.nvim_set_keymap('i', '<Tab>', 'v:lua.smart_untab()', { noremap = true, expr = true })

vim.api.nvim_set_keymap('n', '<Leader><Leader>','<C-^>', { noremap = true })
vim.api.nvim_set_keymap('', '<Leader>m','ct_', { noremap = true })


vim.cmd('command! -bang Config :e ~/.config/nvim/init.lua')

vim.api.nvim_command([[
	call Base16hi("LspDiagnosticsSignError", g:base16_gui08, "", g:base16_cterm08, "", "", "")
	call Base16hi("LspDiagnosticsSignWarning", g:base16_gui09, "", g:base16_cterm09, "", "", "")
	call Base16hi("LspDiagnosticsSignInfo", g:base16_gui0D, "", g:base16_cterm0D, "", "", "")
	call Base16hi("LspDiagnosticsSignHint", g:base16_gui0C, "", g:base16_cterm0C, "", "", "")
		 
	call Base16hi("LspDiagnosticsVirtualTextError", g:base16_gui08, "", g:base16_cterm08, "", "", "")
	call Base16hi("LspDiagnosticsVirtualTextWarning", g:base16_gui09, "", g:base16_cterm09, "", "", "")
	call Base16hi("LspDiagnosticsVirtualTextInfo", g:base16_gui0D, "", g:base16_cterm0D, "", "", "")
	call Base16hi("LspDiagnosticsVirtualTextHint", g:base16_gui0C, "", g:base16_cterm0C, "", "", "")
	
	call Base16hi("LspDiagnosticsFloatingError", g:base16_gui08, g:base16_gui02, g:base16_cterm08, g:base16_cterm02, "", "")
	call Base16hi("LspDiagnosticsFloatingWarning", g:base16_gui09, g:base16_gui02, g:base16_cterm09, g:base16_cterm02, "", "")
	call Base16hi("LspDiagnosticsFloatingInfo", g:base16_gui0D, g:base16_gui02, g:base16_cterm0D, g:base16_cterm02, "", "")
	call Base16hi("LspDiagnosticsFloatingHint", g:base16_gui0C, g:base16_gui02, g:base16_cterm0C, g:base16_cterm02, "", "")
	
	call Base16hi("LspDiagnosticsUnderlineError", g:base16_gui08, g:base16_gui01, g:base16_cterm08, g:base16_cterm01, "underline", "")
	call Base16hi("LspDiagnosticsUnderlineWarning", g:base16_gui09, g:base16_gui01, g:base16_cterm09, g:base16_cterm01, "underline", "")
	call Base16hi("LspDiagnosticsUnderlineInfo", g:base16_gui0D, g:base16_gui01, g:base16_cterm0D, g:base16_cterm01, "underline", "")
	call Base16hi("LspDiagnosticsUnderlineHint", g:base16_gui0C, g:base16_gui01, g:base16_cterm0C, g:base16_cterm01, "underline", "")

	call Base16hi("LsoReferenceText", g:base16_gui01, g:base16_gui0A, g:base16_cterm01, g:base16_cterm0A, "", "")
	call Base16hi("LsoReferenceRead", g:base16_gui01, g:base16_gui0B, g:base16_cterm01, g:base16_cterm0B, "", "")
	call Base16hi("LsoReferenceWrite", g:base16_gui01, g:base16_gui08, g:base16_cterm01, g:base16_cterm08, "", "")
]])
