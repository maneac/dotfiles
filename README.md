# Dotfiles

Dotfiles repo

## Installation

### Prerequisites

- git
- tmux
- nvim
- zsh

**Arch**:

```sh
sudo pacman -Syu
sudo pacman -S neovim git tmux
```

**Ubuntu / Debian**:

```sh
sudo apt update
sudo apt install neovim git tmux
```

### Additional Tools

**All**:

```sh
curl -fsSL "https://sh.rustup.rs" | sh

$HOME/.cargo/bin/cargo install exa ripgrep starship bat du-dust

git clone --depth=1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install

git clone --depth=1 https://github.com/chriskempson/base16-shell.git ~/.config/base16-shell
```

**Arch**:

```sh
$HOME/.cargo/bin install paru
```

### Setup

```sh
cd $HOME
git clone --bare https://gitlab.com/maneac/dotfiles.git $HOME/.dotfiles-git
git --git-dir=$HOME/.dotfiles-git --work-tree=$HOME checkout
git --git-dir=$HOME/.dotfiles-git --work-tree=$HOME config --local status.showUntrackedFiles no

base16_gruvbox-dark-medium
nvim +PlugUpdate +qa
```

### Additional Steps

#### Configure SSH

```sh
ssh-keygen -t ed25519
```

Add public key to Git servers.

#### Configure GPG

```sh
gpg --full-gen-key
```

Add PEM to Git servers:

```sh
gpg --list-secret-keys --keyid-format LONG <your_email>

gpg --armor --export <sec>
```

#### Configure Git

Create `.local/.gitconfig` containing local Git configration:

```sh
[user]
    name=""
    username=""
    email=""
    signingkey=""
```

Add `<sec>` to `.local/.gitconfig` as the `signingkey`.

#### Rust Development

Install `rust-analyzer`:

```sh
rustup +nightly component add rust-analyzer-preview
```

#### Go Development

Install `gopls`:

```sh
go install golang.org/x/tools/gopls@latest
```

#### Configure ZSH

Create a `.local/.zshrc.local` containing local configuration.
