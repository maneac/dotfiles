LOCAL="$HOME/.local"

export STARSHIP_CONFIG="$HOME/starship.toml"
eval "$(starship init zsh)"

## History file configuration
[ -z "${HISTFILE:-}" ] && HISTFILE="$LOCAL/.zsh_history"
[ "$HISTSIZE" -lt 50000 ] && HISTSIZE=50000
[ "$SAVEHIST" -lt 10000 ] && SAVEHIST=10000

bindkey "^[[A" history-search-backward
bindkey "^[[B" history-search-forward

# Options

# Directories
setopt auto_pushd
setopt pushd_ignore_dups

# Completion
setopt always_last_prompt
setopt always_to_end
setopt auto_list
setopt auto_menu
setopt auto_param_keys
setopt auto_param_slash
setopt auto_remove_slash
setopt list_ambiguous
setopt list_rows_first
setopt menu_complete
setopt list_types

# History
setopt extended_history
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_no_functions
setopt hist_no_store
setopt hist_reduce_blanks
setopt hist_verify
setopt inc_append_history
setopt share_history

# I/O
setopt aliases
setopt interactive_comments

# Jobs
setopt auto_continue

# Scripts
setopt c_bases
setopt pipe_fail

# Customisation
alias dotfiles='git --git-dir=$HOME/.dotfiles-git --work-tree=$HOME'

alias vi="nvim"
alias vim="nvim"

alias c="clear"

alias ls="exa --icons -l --group-directories-first --no-time --octal-permissions --no-permissions"
alias ll="ls"
alias lls="ls -a"
alias lll="lls"
alias lst="ls -T"
alias ft="exa -FaT --icons"

alias cat="bat --paging=never"
alias grep="rg"

alias gupd='go get -u \$(go list -mod=mod -f "{{if not (or .Main .Indirect)}}{{.Path}}{{end}}" -m all)'
alias gov='go mod tidy; go mod vendor'
alias glint='golangci-lint run -v'

LOCAL_ZSHRC="$LOCAL/.zshrc"
[ -f "$LOCAL_ZSHRC" ] && source "$LOCAL_ZSHRC" 

export FZF_DEFAULT_OPTS='--height 30% --layout=reverse --border'
[ -f "$HOME/.fzf.zsh" ] && source "$HOME/.fzf.zsh"

BASE16_SHELL="$HOME/.config/base16-shell"
[ -n "$PS1" ] && [ -s "$BASE16_SHELL/profile_helper.sh" ] && eval "$("$BASE16_SHELL"/profile_helper.sh)"

export PATH="$PATH:$HOME/.cargo/bin:$HOME/.local/bin"
export TERM=xterm-256color
TTY=$(tty)
export GPG_TTY="$TTY"

